use rand::Rng;
use std::{fs::File, io::prelude::*};

fn main() {
    let stations_raw = include_str!("weather_stations.txt");
    let stations_parsed: Vec<&str> = stations_raw.rsplit('\n').collect();

    let mut file = File::create("data.txt").unwrap();
    for _ in 0..1000 {
        let mut data = String::new();
        for _ in 0..1_000_000 {
            let city = rand::thread_rng().gen_range(0..stations_parsed.len());
            let temp = rand::thread_rng().gen_range(-99.0..99.0);

            data.push_str(&format!("{};{:.1}\n", stations_parsed[city], temp));
        }
        file.write(data.as_bytes()).unwrap();
    }
}
