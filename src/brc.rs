use std::{
    collections::HashMap,
    fs::File,
    io::{self, BufRead},
    path::Path,
};

#[derive(Debug)]
struct Station {
    total: f32,
    min: f32,
    max: f32,
    count: u32,
}

impl Station {
    fn new(entry: f32) -> Self {
        Station {
            total: entry,
            min: entry,
            max: entry,
            count: 1,
        }
    }

    fn update(&self, entry: f32) -> Self {
        Station {
            total: self.total + entry,
            min: if entry > self.min { self.min } else { entry },
            max: if entry < self.max { self.max } else { entry },
            count: self.count + 1,
        }
    }

    fn average(&self) -> f32 {
        self.total / (self.count as f32)
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn main() {
    let mut collection: HashMap<String, Station> = HashMap::new();

    if let Ok(lines) = read_lines(std::env::var("FILE").unwrap()) {
        for line in lines.flatten() {
            let entry: Vec<&str> = line.split(';').collect();
            let name = entry[0];
            let value: f32 = entry[1].parse().unwrap();

            match collection.get(name) {
                None => collection.insert(name.to_string(), Station::new(value)),
                Some(e) => collection.insert(name.to_string(), e.update(value)),
            };
        }
    }

    for entry in collection {
        println!(
            "{};{};{};{}",
            entry.0,
            entry.1.min,
            entry.1.average(),
            entry.1.max
        );
    }
}
